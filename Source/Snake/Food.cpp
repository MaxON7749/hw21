// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Bonus.h"
#include "AntiBonus.h"
#include "Math/UnrealMathUtility.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshFoodComp"));
	Counter = 5;
	AntiCounter = 7;


}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead) 
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			
		}
		float xRand = FMath::FRandRange(-350.f, 350.f);
		float yRand = FMath::FRandRange(-350.f, 350.f);
		SetActorLocation(FVector(xRand, yRand, 0));
		Counter--;
		AntiCounter--;
	}

	if (Counter <= 0)
	{
		SpawnBonus();
		Counter = 5;
	}
	
	if (AntiCounter <= 0)
	{
		SpawnAntiBonus();
		AntiCounter = 7;
	}

}

void AFood::SpawnBonus()
{
	float xRandBonus = FMath::FRandRange(-350.f, 350.f);
	float yRandBonus = FMath::FRandRange(-350.f, 350.f);
	FVector BonusVector(xRandBonus, yRandBonus, 0);
	FTransform NewBonusTransf(BonusVector);
	BonusAddSpeed = GetWorld()->SpawnActor<ABonus>(BonusClass, NewBonusTransf);
}

void AFood::SpawnAntiBonus()
{
	float xRandAntiBonus = FMath::FRandRange(-350.f, 350.f);
	float yRandAntiBonus = FMath::FRandRange(-350.f, 350.f);
	FVector AntiBonusVector(xRandAntiBonus, yRandAntiBonus, 0);
	FTransform NewAntiBonusTranform(AntiBonusVector);
	AntiBonusRemoveSpeed = GetWorld()->SpawnActor<AAntiBonus>(AntiBonusClass, NewAntiBonusTranform);
}

