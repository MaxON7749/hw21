// Fill out your copyright notice in the Description page of Project Settings.


#include "AntiBonus.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"


// Sets default values
AAntiBonus::AAntiBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AntiBonusCube = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("AntiBonusStaticMesh"));

}

// Called when the game starts or when spawned
void AAntiBonus::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAntiBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAntiBonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->RemoveSpeed(0.1f);
		}
	}
	Destroy();
}

