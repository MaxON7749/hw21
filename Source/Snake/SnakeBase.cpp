// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Interacteble.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 0.5f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i) 
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex=SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
			
		}
	}
	
	SnakeElements[SnakeElements.Num()-1]->MeshComponent->SetVisibility(false);
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= ElementSize;
		break;
	}

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	for (int i = SnakeElements.Num()-1; i>0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i-1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		
	}
	SnakeElements[SnakeElements.Num() - 1]->MeshComponent->SetVisibility(true);
	SnakeElements[0]->AddActorWorldOffset(MovementVector);

	SnakeElements[0]->ToggleCollision();
	
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedBlock, AActor* Other)
{
	if (IsValid(OverlappedBlock))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedBlock, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteracteble* InteractebleInterface = Cast<IInteracteble>(Other);
		if (InteractebleInterface)
		{
			InteractebleInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::AddSpeed(float AddMovmentSpeed)
{
	MovementSpeed = MovementSpeed - AddMovmentSpeed;
	if (MovementSpeed <= 0) {
		MovementSpeed = 0.1f;
	}
	SetActorTickInterval(MovementSpeed);
}

void ASnakeBase::RemoveSpeed(float RemoveMovmentSpeed)
{
	MovementSpeed = MovementSpeed + RemoveMovmentSpeed;
	if (MovementSpeed >= 1) {
		MovementSpeed = 0.9f;
	}
	SetActorTickInterval(MovementSpeed);
}

