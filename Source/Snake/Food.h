// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interacteble.h"
#include "Food.generated.h"

class UStaticMeshComponent;
class ABonus;
class AAntiBonus;

UCLASS()
class SNAKE_API AFood : public AActor, public IInteracteble
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(BlueprintReadWrite)
	ABonus* BonusAddSpeed;

	UPROPERTY(BlueprintReadWrite)
	AAntiBonus* AntiBonusRemoveSpeed;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABonus> BonusClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AAntiBonus> AntiBonusClass;

	UPROPERTY()
	int Counter;

	UPROPERTY(EditDefaultsOnly)
	int AntiCounter;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	void SpawnBonus();
	
	void SpawnAntiBonus();


};
